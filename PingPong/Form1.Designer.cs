﻿namespace AB02
{
    partial class frmPingPong
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnSpielstarten = new System.Windows.Forms.Button();
            this.Spieltimer = new System.Windows.Forms.Timer(this.components);
            this.lblTime = new System.Windows.Forms.Label();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblPunkte = new System.Windows.Forms.Label();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.grpSteuerung = new System.Windows.Forms.GroupBox();
            this.rbtnSchlägersteuerung = new System.Windows.Forms.RadioButton();
            this.btnBallsteuerung = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBall)).BeginInit();
            this.grpSteuerung.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.SeaGreen;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.panel1);
            this.pnlSpiel.Controls.Add(this.picBall);
            this.pnlSpiel.Location = new System.Drawing.Point(30, 30);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(550, 450);
            this.pnlSpiel.TabIndex = 0;
            this.pnlSpiel.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSpiel_Paint);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(537, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(9, 37);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.picBall.Location = new System.Drawing.Point(100, 150);
            this.picBall.Margin = new System.Windows.Forms.Padding(0);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(25, 25);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            this.picBall.Click += new System.EventHandler(this.picBall_Click);
            // 
            // btnSpielstarten
            // 
            this.btnSpielstarten.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSpielstarten.Location = new System.Drawing.Point(190, 487);
            this.btnSpielstarten.Name = "btnSpielstarten";
            this.btnSpielstarten.Size = new System.Drawing.Size(227, 23);
            this.btnSpielstarten.TabIndex = 1;
            this.btnSpielstarten.Text = "Spiel starten";
            this.btnSpielstarten.UseVisualStyleBackColor = false;
            this.btnSpielstarten.Click += new System.EventHandler(this.btnSpielstarten_Click);
            // 
            // Spieltimer
            // 
            this.Spieltimer.Interval = 20;
            this.Spieltimer.Tick += new System.EventHandler(this.Spieltimer_Tick);
            // 
            // lblTime
            // 
            this.lblTime.Location = new System.Drawing.Point(200, 4);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(100, 23);
            this.lblTime.TabIndex = 2;
            this.lblTime.Text = "Time";
            this.lblTime.Click += new System.EventHandler(this.lblTime_Click);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(582, 30);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(31, 450);
            this.vScrollBar1.TabIndex = 1;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(64, 487);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblPunkte
            // 
            this.lblPunkte.Location = new System.Drawing.Point(19, 487);
            this.lblPunkte.Name = "lblPunkte";
            this.lblPunkte.Size = new System.Drawing.Size(40, 20);
            this.lblPunkte.TabIndex = 4;
            this.lblPunkte.Text = "Punkte:";
            this.lblPunkte.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(711, 245);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 23);
            this.btnDown.TabIndex = 7;
            this.btnDown.Text = "Down";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(616, 206);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 8;
            this.btnLeft.Text = "Left";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnUp.Location = new System.Drawing.Point(711, 163);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 23);
            this.btnUp.TabIndex = 9;
            this.btnUp.Text = "Up";
            this.btnUp.UseVisualStyleBackColor = false;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(807, 206);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 10;
            this.btnRight.Text = "Right";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(743, 291);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 134);
            this.label1.TabIndex = 11;
            this.label1.Text = "Tastensteuerung: Taste H ==> horizontale Flugrichtung umkehren; V ==> vertikale F" +
    "lugrichtung umkehren; P ==> Spiel pausieren; S ==> Spiel weiterlaufen lassen";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // grpSteuerung
            // 
            this.grpSteuerung.Controls.Add(this.rbtnSchlägersteuerung);
            this.grpSteuerung.Controls.Add(this.btnBallsteuerung);
            this.grpSteuerung.Location = new System.Drawing.Point(616, 401);
            this.grpSteuerung.Name = "grpSteuerung";
            this.grpSteuerung.Size = new System.Drawing.Size(170, 56);
            this.grpSteuerung.TabIndex = 13;
            this.grpSteuerung.TabStop = false;
            this.grpSteuerung.Text = "Wahl der Steuerung";
            this.grpSteuerung.Enter += new System.EventHandler(this.grpSteuerung_Enter);
            // 
            // rbtnSchlägersteuerung
            // 
            this.rbtnSchlägersteuerung.AutoSize = true;
            this.rbtnSchlägersteuerung.Checked = true;
            this.rbtnSchlägersteuerung.Location = new System.Drawing.Point(3, 39);
            this.rbtnSchlägersteuerung.Name = "rbtnSchlägersteuerung";
            this.rbtnSchlägersteuerung.Size = new System.Drawing.Size(114, 17);
            this.rbtnSchlägersteuerung.TabIndex = 1;
            this.rbtnSchlägersteuerung.TabStop = true;
            this.rbtnSchlägersteuerung.Text = "Schlägersteuerung";
            this.rbtnSchlägersteuerung.UseVisualStyleBackColor = true;
            this.rbtnSchlägersteuerung.CheckedChanged += new System.EventHandler(this.rbtnSchlägersteuerung_CheckedChanged);
            // 
            // btnBallsteuerung
            // 
            this.btnBallsteuerung.AutoSize = true;
            this.btnBallsteuerung.Location = new System.Drawing.Point(3, 16);
            this.btnBallsteuerung.Name = "btnBallsteuerung";
            this.btnBallsteuerung.Size = new System.Drawing.Size(89, 17);
            this.btnBallsteuerung.TabIndex = 0;
            this.btnBallsteuerung.TabStop = true;
            this.btnBallsteuerung.Text = "Ballsteuerung";
            this.btnBallsteuerung.UseVisualStyleBackColor = true;
            this.btnBallsteuerung.CheckedChanged += new System.EventHandler(this.btnBallsteuerung_CheckedChanged);
            // 
            // frmPingPong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 513);
            this.Controls.Add(this.grpSteuerung);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.lblPunkte);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.btnSpielstarten);
            this.Controls.Add(this.pnlSpiel);
            this.Name = "frmPingPong";
            this.Text = "Ping-PongSpiel";
            this.Load += new System.EventHandler(this.frmPingPong_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBall)).EndInit();
            this.grpSteuerung.ResumeLayout(false);
            this.grpSteuerung.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button btnSpielstarten;
        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Timer Spieltimer;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPunkte;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpSteuerung;
        private System.Windows.Forms.RadioButton rbtnSchlägersteuerung;
        private System.Windows.Forms.RadioButton btnBallsteuerung;
    }
}

