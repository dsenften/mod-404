﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AB02
{
    
    public partial class frmGameOver : Form
    {
        //verschiedene Variablen werden vergeben
        static string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        public string punkte2;
        string filename = @"C:\Users\" + username + "test.txt";
        public frmGameOver()
        {
            InitializeComponent();
        }

        private void frmGameOver_Load(object sender, EventArgs e)
        {
            //Punkte festlegen; die File zum Abspeichern und zu Createn
            lblPunkte.Text = punkte2;

            frmPingPong.points = 0;

            if (!File.Exists(filename))
                {
                FileStream createFile = File.Create(filename);
                
                   // var file = File.Create(filename);
                    createFile.Close();

                }
                if (File.Exists(filename))
                {
                    Open(filename);
                }
            
        }

        private void Open(string filename)
        {
            //einfügen des Namens und der erreichten Punkte
            StreamReader readfile = new StreamReader(filename);
            string Ergebnisse = readfile.ReadToEnd();
            lblErgebnisse.Text = Ergebnisse;
            readfile.Close();
        }


        private void lblPunkte_Click(object sender, EventArgs e)
        {

        }

        private void btnEintragen_Click(object sender, EventArgs e)
        {
            //Speicherung des Files
            Open(filename);
            StreamWriter TEXT = new StreamWriter(filename);
            string eingabe = lblErgebnisse.Text + " " + lblPunkte.Text + " " + txtName.Text;
            TEXT.WriteLine(eingabe);
            TEXT.Close();                        
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnSchliessen_Click(object sender, EventArgs e)
        {
            // Zurück auf den Start der Form1
            Close();
        }
    }
}
