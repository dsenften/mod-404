﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AB06_SchachbretVersuch4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Zeichneschachbrett();
        }
        public void Zeichneschachbrett()
        {
            int number = Convert.ToInt32(nubAnzahlZeilen.Value);
            pnlSchachbrett.Controls.Clear();
            if (number !=0)
            {
                int lblHeight = pnlSchachbrett.Height / number;
                int lblWidth = pnlSchachbrett.Width / number;
                pnlSchachbrett.Height = lblHeight * number;
                pnlSchachbrett.Width = lblWidth * number;
                label1.Width = lblWidth;
                label1.Height = lblHeight;
                label1.Location = new Point(0, 0);

                for (int i = 0; i < number; i++)
                {
                    for (int s = 0; s < number; s++)
                    {
                        Label lbl = new Label();
                        lbl.Location = new Point(lbl.Location.X + lblWidth * s, lbl.Location.Y +lblHeight * i);
                        lbl.Name = "lblnewlabel" + Convert.ToString(i) + Convert.ToString(s);
                        lbl.Size = new Size(lblWidth, lblHeight);
                        lbl.Text = label1.Text;

                        if(i % 2 == 0)
                        {
                            if(s % 2 == 0)
                            {
                                lbl.BackColor = Color.Black;
                            }
                            else lbl.BackColor = Color.White;
                        }
                        else if(i % 2 == 1)
                        {
                            if(s % 2 == 0)
                            {
                                lbl.BackColor = Color.White;
                            }
                            else lbl.BackColor = Color.Black;
                        }

                        pnlSchachbrett.Controls.Add(lbl);
                    }
                }
            }
        }
    }
}
