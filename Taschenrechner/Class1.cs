﻿using System;
namespace AB01
{

    public class Rechenoperation
    {
        public Class1()
        {
            double Multiplikation(double zahl1, double zahl2)
            {
                return zahl1 * zahl2;
            }
            double Division(double zahl1, double zahl2)
            {
                return zahl1 / zahl2;
            }
            double Potenzierung(double zahl1, double zahl2)
            {
                return Math.Pow(zahl1, zahl2);
            }
            double Subtraktion(double zahl1, double zahl2)
            {
                return zahl1 - zahl2;
            }
            double Addition(double zahl1, double zahl2)
            {
                return zahl1 + zahl2;
            }
            double Mittelwert(double zahl1, double zahl2)
            {
                return (zahl1 + zahl2) / 2;

            }
            double Maximum(double zahl1, double zahl2)
            {
                if (zahl1 > zahl2) return zahl1;
               else return zahl2;
            }
        }
    }
}
